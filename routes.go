package main

import (
	"errors"
	"fmt"
	"image/color"
	"io"
	"net/http"
	"os"
	"path/filepath"
	"strings"
	"time"

	"github.com/julienschmidt/httprouter"
	"github.com/rs/xid"
	"github.com/rs/zerolog/log"
	"gitlab.com/mbecker/gpxs-http-upload/models"
	"gitlab.com/mbecker/gpxs-http-upload/utils"
	apiv9 "gitlab.com/mbecker/gpxs-proto/apiv9"
)

func statusRoute() httprouter.Handle {
	return func(w http.ResponseWriter, r *http.Request, _ httprouter.Params) {
		w.WriteHeader(http.StatusOK)
		w.Header().Add("Content-Type", "application/json")
		response := map[string]interface{}{"status": true, "message": "Status OK"}
		utils.Respond(w, response)
		return
	}
}

type MapTexts struct {
	Text1 TextColor
	Text2 TextColor
	Text3 TextColor
}

type TextColor struct {
	Color     string
	ColorRGBA *color.RGBA
	Text      string
}

func upload() httprouter.Handle {
	return func(w http.ResponseWriter, r *http.Request, _ httprouter.Params) {

		log.Info().Str("http", r.Method).Str("route", "upload").Msg("--- UPLOAD ---")
		fmt.Println("--- UPLOAD ----")

		// CORS does pre-flight requests sending an OPTIONS request to any URL, so to handle a POST request you first need to handle an OPTIONS request to that same URL.
		if (*r).Method == "OPTIONS" {
			return
		}

		if (*r).Method != "POST" {
			// Not supported HTTP method
			log.Info().Str("http method", r.Method).Msg("Unknwon http method")
			w.WriteHeader(http.StatusInternalServerError)
			return
		}

		// XIDs: Create identifier for gpx file and image
		xidMapOriginal := xid.New()
		xidMap := xidMapOriginal.String()

		response := make(map[string]interface{})
		uuid := r.Context().Value(models.KeyUUID).(string)

		defer utils.TimeTrack(time.Now(), "upload", xidMap, uuid)

		r.ParseMultipartForm(32 << 20)
		for key, value := range r.Form {
			fmt.Printf("%s - %s\n", key, value)
		}

		// Check file metafiels
		metadataActivityname := r.FormValue("activityname")
		metadataActivityType := r.FormValue("activitytype")
		metadataPrivacy := strings.Split(r.FormValue("privacy"), ",")
		metadataImagePreset := r.FormValue("imagepreset")
		metadataTileprovider := r.FormValue("tileset")

		mapTexts := MapTexts{
			Text1: TextColor{
				Text:  r.FormValue("text1"),
				Color: r.FormValue("color1"),
			},
			Text2: TextColor{
				Text:  r.FormValue("text2"),
				Color: r.FormValue("color2"),
			},
			Text3: TextColor{
				Text:  r.FormValue("text3"),
				Color: r.FormValue("color3"),
			},
		}

		c1, err := ParseHexColorFast(mapTexts.Text1.Color)
		if err == nil {
			mapTexts.Text1.ColorRGBA = &c1
		}
		c2, err := ParseHexColorFast(mapTexts.Text2.Color)
		if err == nil {
			mapTexts.Text2.ColorRGBA = &c2
		}
		c3, err := ParseHexColorFast(mapTexts.Text3.Color)
		if err == nil {
			mapTexts.Text3.ColorRGBA = &c3
		}

		imagePresetType, err := getImagePresetType(metadataImagePreset)

		// fmt.Println("--- (1) ----")
		// fmt.Println(metadataActivityname)
		// fmt.Println(metadataActivityType)
		// fmt.Println(metadataPrivacy)
		// fmt.Println(imagePresetType)
		// fmt.Println(metadataTileprovider)
		// fmt.Println("----------")

		if err != nil || len(metadataPrivacy) == 0 || metadataImagePreset == "" || metadataTileprovider == "" {
			log.Info().Str("xid", xidMap).Str("uuid", uuid).Str("name", "upload").Msg("Metadata Privacy, ImagePreset and/or Tileset empty")
			response = utils.Message(false, "Metadata Privacy and/or ImagePreset empty")
			w.WriteHeader(http.StatusInternalServerError)
			w.Header().Add("Content-Type", "application/json")
			utils.Respond(w, response)
			return
		}

		tileprovider := getTileprovider(metadataTileprovider)
		activityTpe := getActivityTpe(metadataActivityType)
		privacyType := getPrivacy(metadataPrivacy)

		/**
		 * TODO:
		 * - [x] Add queue to route handler
		 */
		// if !queue.IsConnected || queue.IsClosing {
		// 	log.Error().Err(errors.New("AMQP Connection closed")).Str("xid", xidMap).Str("uuid", uuid).Str("name", "upload").Msg("AMQP connection closed")
		// 	response = utils.Message(false, "Internal Server Error")
		// 	w.WriteHeader(http.StatusInternalServerError)
		// 	w.Header().Add("Content-Type", "application/json")
		// 	utils.Respond(w, response)
		// 	return
		// }

		// HTTP method "POST"
		file, handler, err := r.FormFile("file")
		if err != nil {
			log.Error().Err(err).Str("xid", xidMap).Str("uuid", uuid).Str("name", "upload").Msg("Directory uuid couldn't be created")
			response = utils.Message(false, err.Error())
			w.WriteHeader(http.StatusInternalServerError)
			w.Header().Add("Content-Type", "application/json")
			utils.Respond(w, response)
			return
		}
		defer file.Close()

		// Check that file is a *.gpx file
		if filepath.Ext(handler.Filename) != ".gpx" {
			log.Info().Err(err).Str("xid", xidMap).Str("uuid", uuid).Str("name", "upload").Msg("Directory uuid couldn't be created")
			response = utils.Message(false, "The uploaded file is not a gpx file")
			w.WriteHeader(http.StatusBadRequest)
			w.Header().Add("Content-Type", "application/json")
			utils.Respond(w, response)
			return
		}

		// Create "new" directory for uuid
		uploadDirectoryUUID := filepath.Join(uploadDirectory, uuid)
		err = utils.CreateDirIfNotExist(uploadDirectoryUUID)
		if err != nil {
			log.Error().Err(err).Str("xid", xidMap).Str("uuid", uuid).Str("name", "upload").Msg("Directory uuid couldn't be created")
			response = utils.Message(false, err.Error())
			w.WriteHeader(http.StatusInternalServerError)
			w.Header().Add("Content-Type", "application/json")
			utils.Respond(w, response)
			return
		}

		// Write file to direcory
		// The filename is defined as follows: "xidGpx.gpx"
		fileName := fmt.Sprintf("%s.gpx", xidMap)
		filePath := filepath.Join(uploadDirectoryUUID, fileName)
		f, err := os.OpenFile(filePath, os.O_WRONLY|os.O_CREATE, 0666)
		if err != nil {
			log.Error().Err(err).Str("xid", xidMap).Str("uuid", uuid).Str("name", "upload").Msg("File couldn't be created")
			response = utils.Message(false, err.Error())
			w.WriteHeader(http.StatusInternalServerError)
			w.Header().Add("Content-Type", "application/json")
			utils.Respond(w, response)
			return
		}
		defer f.Close()
		io.Copy(f, file)

		/*
		 * File is uploaded:
		 * - Create GPX Proto message
		 * - Send proto messaget to AMQP
		 */
		/**
		 * TODO:
		 * - [ ] Add internal work queue to retry to send message
		 * - [ ] Delete file if DB was not successful
		 */

		fmt.Println("--> All checks successful")
		fmt.Println("--> SQL Insert")

		sendMesseage(123456789, xidMapOriginal, uuid, filePath, imagePresetType, tileprovider, activityTpe, metadataActivityname, privacyType, mapTexts)

		// gpxID, err := sqlInsert(db, xidMap, uuid, fileName, filePath)
		// if err != nil {
		// 	log.Error().Err(err).Str("xid", xidMap).Str("uuid", uuid).Str("name", "sql insert gpx and map").Msg("SQL Exec not successful")
		// 	response = utils.Message(false, "Database connection error")
		// 	w.WriteHeader(http.StatusInternalServerError)
		// 	w.Header().Add("Content-Type", "application/json")
		// 	utils.Respond(w, response)
		// 	return
		// }

		// response = utils.MessageSuccess("Upload successful", gpxID, xidMap)

		// utils.Respond(w, map[string]interface{}{
		// 	"image":                fmt.Sprintf("/maps/%s.gpx.jpeg", xidMap),
		// 	"xidMap":               xidMap,
		// 	"fileName":             fileName,
		// 	"filePath":             filePath,
		// 	"imagePresetType":      imagePresetType,
		// 	"tileprovider":         tileprovider,
		// 	"activityTpe":          activityTpe,
		// 	"metadataActivityname": metadataActivityname,
		// 	"privacyType":          privacyType,
		// })

		var redirectURL = fmt.Sprintf("/maps/%s.gpx", xidMap)
		if imagePresetType == apiv9.ImagePresetType_Instagram {
			redirectURL = fmt.Sprintf("%s.jpeg", redirectURL)
		} else {
			redirectURL = fmt.Sprintf("%s.png", redirectURL)
		}
		http.Redirect(w, r, redirectURL, 301)

		// tmpl := template.Must(template.ParseFiles("./templates/upload.html"))
		// data := UploadData{
		// 	URL: fmt.Sprintf("/maps/%s.gpx.jpeg", xidMap),
		// }
		// tmpl.Execute(w, data)

		return

	}
}

type UploadData struct {
	URL string
}

func getImagePresetType(imagePresetTypeAsString string) (apiv9.ImagePresetType, error) {
	var imagePresetType apiv9.ImagePresetType
	imagePresetTypeAsString = strings.ToLower(imagePresetTypeAsString)
	if imagePresetTypeAsString == strings.ToLower(apiv9.ImagePresetType_name[0]) {
		imagePresetType = apiv9.ImagePresetType_iPhone8
		return imagePresetType, nil
	}
	if imagePresetTypeAsString == strings.ToLower(apiv9.ImagePresetType_name[1]) {
		imagePresetType = apiv9.ImagePresetType_iPhone8Plus
		return imagePresetType, nil
	}
	if imagePresetTypeAsString == strings.ToLower(apiv9.ImagePresetType_name[2]) {
		imagePresetType = apiv9.ImagePresetType_iPadPro129inch
		return imagePresetType, nil
	}
	if imagePresetTypeAsString == strings.ToLower(apiv9.ImagePresetType_name[3]) {
		imagePresetType = apiv9.ImagePresetType_Instagram
		return imagePresetType, nil
	}
	return imagePresetType, errors.New("No ImageType found")
}

func getTileprovider(tileproviderAsString string) apiv9.TileProviderType {
	var tileprovider apiv9.TileProviderType
	switch tileproviderAsString {
	case "dark":
		tileprovider = apiv9.TileProviderType_CartoDark
	case "light":
		tileprovider = apiv9.TileProviderType_CartoLight
	case "street":
		tileprovider = apiv9.TileProviderType_OpenStreetMaps
	case "watercolor":
		tileprovider = apiv9.TileProviderType_StamenWatercolor
	default:
		tileprovider = apiv9.TileProviderType_CartoDark
	}
	return tileprovider
}

func getActivityTpe(activityTpeAsString string) apiv9.ActivityType {
	var activityTpe apiv9.ActivityType
	switch activityTpeAsString {
	case "running":
		activityTpe = apiv9.ActivityType_Running
	case "cycling":
		activityTpe = apiv9.ActivityType_Cycling
	case "hiking":
		activityTpe = apiv9.ActivityType_Hiking
	case "walking":
		activityTpe = apiv9.ActivityType_Walking
	default:
		activityTpe = apiv9.ActivityType_Default
	}
	return activityTpe
}

func getPrivacy(privacyAsSeeting []string) []apiv9.PrivacyType {
	privacy := []apiv9.PrivacyType{}
	for _, el := range privacyAsSeeting {
		if el == "private" {
			privacy = append(privacy, apiv9.PrivacyType_Private)
			return privacy
		}
	}
	privacy = append(privacy, apiv9.PrivacyType_Public)
	privacy = append(privacy, apiv9.PrivacyType_Community)
	return privacy
}
