package models

import jwt "github.com/dgrijalva/jwt-go"

type key int

func (k key) String() string {
	switch k {
	case KeyUUID:
		return string(k)
	default:
		return string(k)
	}
}

const (
	KeyUUID key = iota
	// ...
)

/*
JWT claims struct
*/
type Token struct {
	UUID string
	jwt.StandardClaims
}
