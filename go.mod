module gitlab.com/mbecker/gpxs-http-upload

go 1.15

require (
	github.com/alexsasharegan/dotenv v0.0.0-20171113213728-090a4d1b5d42
	github.com/dgrijalva/jwt-go v3.2.0+incompatible
	github.com/flopp/go-staticmaps v0.0.0-20211231152539-56e3560e444b
	github.com/fogleman/gg v1.3.0
	github.com/golang/freetype v0.0.0-20170609003504-e2365dfdc4a0
	github.com/golang/geo v0.0.0-20210211234256-740aa86cb551
	github.com/golang/protobuf v1.2.0
	github.com/google/uuid v1.3.0
	github.com/julienschmidt/httprouter v1.3.0
	github.com/lib/pq v1.0.0
	github.com/lucasb-eyer/go-colorful v1.2.0
	github.com/mbecker/gpxs v0.0.0-20190125134801-5c54f7526f28
	github.com/nfnt/resize v0.0.0-20180221191011-83c6a9932646
	github.com/rs/xid v1.3.0
	github.com/rs/zerolog v1.10.3
	github.com/satori/go.uuid v1.2.0
	github.com/streadway/amqp v0.0.0-20181107104731-27835f1a64e9
	gitlab.com/mbecker/gpxs-amqp-lib v0.0.0-20190220111439-21ac52c0a103
	gitlab.com/mbecker/gpxs-proto v0.0.0-20190223195342-c1c3aef3ffea
	golang.org/x/image v0.0.0-20211028202545-6944b10bf410 // indirect
)
