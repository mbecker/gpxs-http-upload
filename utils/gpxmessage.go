package utils

import (
	"time"

	"github.com/golang/protobuf/ptypes/timestamp"
	"github.com/rs/xid"
	apiv9 "gitlab.com/mbecker/gpxs-proto/apiv9"
)

var tileProviders = [...]apiv9.TileProviderType{
	apiv9.TileProviderType_CartoDark,
	apiv9.TileProviderType_CartoLight,
	apiv9.TileProviderType_OpenCycleMap,
	apiv9.TileProviderType_OpenStreetMaps,
	apiv9.TileProviderType_OpenTopoMap,
	apiv9.TileProviderType_StamenTerrain,
	apiv9.TileProviderType_StamenToner,
	apiv9.TileProviderType_ThunderforestLandscape,
	apiv9.TileProviderType_ThunderforestOutdoors,
	apiv9.TileProviderType_ThunderforestTransport,
	apiv9.TileProviderType_Wikimedia,
	apiv9.TileProviderType_StamenWatercolor,
}

// GpxMessage creates an AMQP Proto Message for the GpxType_Gpxs
func GpxMessage(gpxID int64, xidMap xid.ID, uuid string, filePath string, activityName string, activityTpe apiv9.ActivityType, imagePreset apiv9.ImagePresetType, tileProvider apiv9.TileProviderType, privacy []apiv9.PrivacyType) (*apiv9.Activity, error) {

	message := &apiv9.Activity{
		GpxType:          apiv9.GpxType_Gpxs,
		Uuid:             uuid,
		ActivityID:       gpxID,
		MapID:            xidMap.String(),
		ActivityName:     activityName,
		ActivityType:     activityTpe,
		ImagePresetType:  imagePreset,
		TileProviderType: tileProvider,
		CreatedAt: &timestamp.Timestamp{
			Seconds: time.Now().Unix(),
		},
		GpxsActivity: &apiv9.GpxsActivity{
			Filename: filePath,
		},
	}

	return message, nil

	// var data []byte
	// data, err := proto.Marshal(message)
	// if err != nil {
	// 	return data, err
	// }
	// return data, nil

}
