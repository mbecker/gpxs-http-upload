package amqpqueue

import (
	"errors"
	"time"

	"github.com/rs/zerolog/log"
	"github.com/streadway/amqp"
)

/**
 * https://gist.github.com/harrisonturton/c6b62d45e6117d5d03ff44e4e8e1e7f7
 */

// Queue represents a connection to a specific queue.
type Queue struct {
	Name          string
	Connection    *amqp.Connection
	Channel       *amqp.Channel
	Done          chan bool
	NotifyClose   chan *amqp.Error
	NotifyConfirm chan amqp.Confirmation
	IsConnected   bool
	IsClosing     bool
	Consumers     []MessageConsumer
}

type MessageConsumer func(*amqp.Delivery)

const (
	// When reconnecting to the server after connection failure
	ReconnectDelay = 5 * time.Second

	// When resending messages the server didn't confirm
	ResendDelay = 5 * time.Second
)

var (
	errNotConnected  = errors.New("not connected to the queue")
	errNotConfirmed  = errors.New("message not confirmed")
	errAlreadyClosed = errors.New("already closed: not connected to the queue")
)

// New creates a new queue instance, and automatically
// attempts to connect to the server.
func New(name string, addr string) *Queue {
	queue := Queue{
		Name: name,
		Done: make(chan bool),
	}
	go queue.handleReconnect(addr)
	return &queue
}

// handleReconnect will wait for a connection error on
// NotifyClose, and then continously attempt to reconnect.
func (queue *Queue) handleReconnect(addr string) {
	for {
		queue.IsConnected = false
		log.Info().Str("func", "amqp").Msg("Attempting to connect")
		if !queue.IsClosing {
			for !queue.connect(addr) {
				log.Info().Str("func", "amqp").Msg("Failed to connect. Retrying.")
				time.Sleep(ReconnectDelay)
			}
		} else {
			time.Sleep(ReconnectDelay)
		}

		select {
		case <-queue.Done:
			log.Info().Str("func", "amqp").Msg("DONE !!!!!")
			return
		case <-queue.NotifyClose:
			queue.IsClosing = true
			log.Info().Str("func", "amqp").Msg("NotifyClose. AMQP is closing")
		}
	}
}

// connect will make a single attempt to connect to
// RabbitMQ. It returns the success of the attempt.
func (queue *Queue) connect(addr string) bool {
	conn, err := amqp.Dial(addr)
	if err != nil {
		return false
	}
	ch, err := conn.Channel()
	if err != nil {
		return false
	}
	ch.Confirm(false)
	_, err = ch.QueueDeclare(
		queue.Name,
		true,  // Durable - Needed for worker
		false, // Delete when unused
		false, // Exclusive
		false, // No-wait
		nil,   // Arguments
	)
	if err != nil {
		return false
	}
	queue.changeConnection(conn, ch)
	queue.IsConnected = true
	log.Info().Str("func", "amqp").Msg("AMQP Connected")

	log.Info().Str("func", "amqp").Str("method", "connect").Str("action", "Recovering MessageConsumers").Str("status", "start").Msg("Recovering MessageConsumers")
	queue.recoverConsumers()
	log.Info().Str("func", "amqp").Str("method", "connect").Str("action", "Recovering MessageConsumers").Str("status", "success").Msg("Recovering MessageConsumers success")

	return true
}

// changeConnection takes a new connection to the queue,
// and updates the channel listeners to reflect this.
func (queue *Queue) changeConnection(connection *amqp.Connection, channel *amqp.Channel) {
	queue.Connection = connection
	queue.Channel = channel
	queue.NotifyClose = make(chan *amqp.Error)
	queue.NotifyConfirm = make(chan amqp.Confirmation)
	queue.Channel.NotifyClose(queue.NotifyClose)
	queue.Channel.NotifyPublish(queue.NotifyConfirm)
}

/**
 * TODO:
 * - [x] add description
 */
func (queue *Queue) SetQos(prefetchCount int, prefetchSize int, global bool) error {
	if queue.Channel == nil {
		return errors.New("Channel is nil")
	}
	return queue.Channel.Qos(
		prefetchCount,
		prefetchSize,
		global,
	)
}

// Push will push data onto the queue, and wait for a confirm.
// If no confirms are recieved until within the resendTimeout,
// it continuously resends messages until a confirm is recieved.
// This will block until the server sends a confirm. Errors are
// only returned if the push action itself fails, see UnsafePush.
func (queue *Queue) Push(data []byte) error {
	if !queue.IsConnected {
		return errors.New("failed to push push: not connected")
	}
	for {
		err := queue.UnsafePush(data)
		if err != nil {
			log.Info().Str("func", "amqp").Msg("Push failed. Retrying.")
			continue
		}
		select {
		case confirm := <-queue.NotifyConfirm:
			if confirm.Ack {
				log.Info().Str("func", "amqp").Msg("Push confirmed.")
				return nil
			}
		case <-time.After(ResendDelay):
		}
		log.Info().Str("func", "amqp").Msg("Push didn't confirm. Retrying")
	}
}

// UnsafePush will push to the queue without checking for
// confirmation. It returns an error if it fails to connect.
// No guarantees are provided for whether the server will
// recieve the message.
func (queue *Queue) UnsafePush(data []byte) error {
	if !queue.IsConnected {
		return errNotConnected
	}
	return queue.Channel.Publish(
		"",         // Exchange
		queue.Name, // Routing key
		false,      // Mandatory
		false,      // Immediate
		amqp.Publishing{
			ContentType: "text/plain",
			Body:        data,
		},
	)
}

// Stream will continuously put queue items on the .
// It is required to call delivery.Ack when it has been
// successfully processed, or delivery.Nack when it fails.
// Ignoring this will cause data to build up on the server.
func (queue *Queue) Stream() (<-chan amqp.Delivery, error) {
	if !queue.IsConnected {
		return nil, errNotConnected
	}
	return queue.Channel.Consume(
		queue.Name,
		"",    // Consumer
		false, // Auto-Ack
		false, // Exclusive
		false, // No-local
		false, // No-Wait
		nil,   // Args
	)
}

// Consume opens an amqp.channel.consume to consume messages and adds the "consumer" to a slice of "consumers" whiche are revoverd when the connection is reconnected
func (queue *Queue) Consume(consumer MessageConsumer) error {
	stream, err := queue.Stream()
	if err != nil {
		return err
	}
	queue.messageConsume(consumer, stream, false)
	return nil
}

// messageConsume adds a consumer MessageConsumer to the slice of message consumer which should be recoverd when the connection is reconnected
// The function returns the amqp.Delivery to the consumer
// amqp.Delivery could be consumes as follows: (d is the amqp.Delivery)
// d.Body
// d.Ack(false) // "false" means: We do not acknowledge multiple messages, just one
func (queue *Queue) messageConsume(consumer MessageConsumer, stream <-chan amqp.Delivery, isRecovered bool) {
	if !isRecovered {
		queue.Consumers = append(queue.Consumers, consumer)
	}
	go func() {
		for amqpDelivery := range stream {
			consumer(&amqpDelivery)
		}
	}()

}

// recoverConsumers recovers all MessageConsumers
func (queue *Queue) recoverConsumers() error {
	for i := range queue.Consumers {
		var consumer = queue.Consumers[i]
		stream, err := queue.Stream()
		if err != nil {
			return err
		}
		queue.messageConsume(consumer, stream, true)
	}
	return nil
}

// Close will cleanly shutdown the channel and connection.
func (queue *Queue) Close() error {

	queue.IsClosing = true
	if !queue.IsConnected {
		queue.IsClosing = false
		return errAlreadyClosed
	}

	err := queue.Channel.Close()
	if err != nil {
		queue.IsClosing = false
		return err
	}
	err = queue.Connection.Close()
	if err != nil {
		queue.IsClosing = false
		return err
	}
	close(queue.Done)
	queue.IsClosing = false
	queue.IsConnected = false
	return nil
}
