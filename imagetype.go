package main

//ImageType represents the type of image as "jpeg", "png", "gif", etc.
type ImageType int

const (
	ImageTypeJpeg ImageType = iota
	ImageTypePng  ImageType = iota
	ImageTypeGif            = iota
)

//ImageTypes includes the values of ImageType
var ImageTypes = [...]string{
	"jpeg",
	"png",
	"gif",
}

func (imageType ImageType) String() string {
	return ImageTypes[imageType]
}

type ResizedImage struct {
	width  float64
	height float64
}
