package utils

import (
	"encoding/json"
	"fmt"
	"net/http"
	"os"
	"time"

	"github.com/rs/zerolog"
	"github.com/rs/zerolog/log"
)

func Message(status bool, message string) map[string]interface{} {
	return map[string]interface{}{"status": status, "message": message}
}
func MessageSuccess(message string, gpxID int64, xidMap string) map[string]interface{} {
	return map[string]interface{}{"status": true, "message": message, "gpxid": gpxID, "xid": xidMap}
}
func Respond(w http.ResponseWriter, data map[string]interface{}) {

	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(http.StatusCreated)
	json.NewEncoder(w).Encode(data)
}

func TimeTrack(start time.Time, name string, xid string, uuid string) {
	duration := time.Since(start)
	log.Debug().Str("xid", xid).
		Str("uuid", uuid).
		Dict("execution", zerolog.Dict().
			Str("name", name).
			Str("duration", fmt.Sprintf("%s", duration)),
		).Msg("Measured execution time")
}

// CreateDirIfNotExist creates a directory (string) if not exists
func CreateDirIfNotExist(dir string) error {
	if _, err := os.Stat(dir); os.IsNotExist(err) {
		err = os.MkdirAll(dir, 0755)
		return err
	}
	return nil
}

func timeTrack(start time.Time, name string, xid string) {
	duration := time.Since(start)
	log.Debug().Str("xid", xid).
		Dict("execution", zerolog.Dict().
			Str("name", name).
			Str("duration", fmt.Sprintf("%s", duration)),
		).Msg("Measured execution time")
}
