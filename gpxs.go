package main

import (
	"fmt"
	"path/filepath"
	"strings"
	"time"

	"github.com/mbecker/gpxs/geo"
	"github.com/mbecker/gpxs/gpxs"
	"github.com/rs/zerolog/log"
	uuid "github.com/satori/go.uuid"
	"gitlab.com/mbecker/gpxs-proto/apiv9"
)

func handleGpxProcess(activityMessage *apiv9.Activity, mapTexts MapTexts) {

	mapID := activityMessage.GetMapID()
	if len(mapID) == 0 {
		log.Warn().Msg("Activity Message: mapID length == 0")
		return
	}

	uuid, err := uuid.FromString(activityMessage.GetUuid())
	if err != nil || len(uuid) == 0 {
		log.Warn().Str("xid", mapID).Msg("GPX Message: UUID is not a valid uuid")
		// return
	}
	activityID := activityMessage.GetActivityID()
	if activityID == 0 {
		log.Warn().Msg("Activity Message: activityID == 0")
		return
	}

	fileName := activityMessage.GpxsActivity.GetFilename()

	activityName := activityMessage.GetActivityName()
	activityColor := activityMessage.GetActivityColor()
	// Privacy
	// isPrivacyPrivate, isPrivacyPublic, isPrivacyCommunity := getPrivacyFromPrivacyType(activityMessage.GetPrivacyType())

	// ActivityType: Default value = GpxMessage_activityTypeDefault
	activityType := activityMessage.GetActivityType()

	// ImagePreset: Default value =
	imagePreset := GetImagePreset(activityMessage.GetImagePresetType())

	// Tileprovider: Default value = GpxMessage_TileProviderCartoDark
	tileProvider := activityMessage.GetTileProviderType()
	imagePreset.tileProvider = TileProviders[tileProvider]

	createdAt := activityMessage.GetCreatedAt()

	if len(fileName) == 0 {
		log.Warn().Str("xid", mapID).Msg("Activity Message: fileName length == 0")
		return // errors.New("GPX Message: Filename's length == 0")
	}

	if createdAt == nil {
		log.Warn().Str("xid", mapID).Msg("Activity Message: Created_At nil")
		return // errors.New("GPX Message: Created_At's length == 0")
	}

	gpxDoc, err := readGpxFile(fileName, mapID)
	if err != nil {
		log.Error().Err(err).Msg("Gpx File couldn't be parsed")
		return //
	}

	// User has not select an option for "activity name" and/or "activity type"
	if len(activityName) == 0 {
		activityName = gpxDoc.Name
	}

	if activityType == apiv9.ActivityType_Default {
		activityType = getActivityTypeString(gpxDoc.Type)
	}

	/**
	 * TODO:
	 * - [] Update filename (directory is mounted; directory "gpx" should be at top level)
	 */

	for k, t := range TileProviders {
		if strings.Contains(t.Name, "LaurisRauszeit") {
			imagePreset.tileProvider = TileProviders[k]
			tileProviderName := TileProviders[k].Name

			// Filepath: Get the base file name ("/dir1/dir2/xid_timestamp.gpx" -> "xid_timestamp.gpx")
			baseFileName := filepath.Base(fileName)                                                               // "xid_timestamp.gpx"
			baseFileNameWithExt := fmt.Sprintf("%s.%s", baseFileName, imagePreset.imageType.String())             // "xid_timestamp.gpx.jpeg"
			baseResizedFileNameWithExt := fmt.Sprintf("%s_%d_%s_resized.jpeg", baseFileName, k, tileProviderName) // "xid_timestamp.gpx_resized.jpeg"
			baseThumbFileNameWithExt := fmt.Sprintf("%s_%d_%s_thumb.jpeg", baseFileName, k, tileProviderName)     // "xid_timestamp.gpx_thumb.jpeg"
			mapImageFilePath := filepath.Join(mapDirectory, baseFileNameWithExt)                                  // "/maps/xid_timestamp.gpx.jpeg"
			mapImageResizedFilePath := filepath.Join(mapDirectory, baseResizedFileNameWithExt)                    // "/maps/xid_timestamp.gpx_resized.jpeg"
			mapThumbFilePath := filepath.Join(mapDirectory, baseThumbFileNameWithExt)                             // "/maps/xid_timestamp.gpx_thumb.jpeg"

			// Labels Activity Stats & Date: "69.02km in 2h45min23s" & "29.11.2018"
			t01, _ := time.ParseDuration(fmt.Sprintf("%ds", int64(gpxDoc.MovementStats.MovingData.Duration)))
			labelActivityStats := fmt.Sprintf("%.2fkm in %s", gpxDoc.MovementStats.OverallData.Distance/1000, t01)
			// labelActivityDate := fmt.Sprintf("%s", gpxDoc.Timestamp)

			labelActivityDate := fmt.Sprintf("%02d.%02d.%d",
				gpxDoc.Timestamp.Day(), gpxDoc.Timestamp.Month(), gpxDoc.Timestamp.Year())

			fmt.Println("MAP CREATE ::: ", k)

			// Eventually consistency: The image could not yet be created but we insert the entry to the DB
			createMap(
				gpxDoc.Tracks,           // Reference to the gpx tracks
				mapImageFilePath,        // Map image file path with extension jpeg|png
				mapImageResizedFilePath, // Resized Map image path with extension jpeg|png
				"",                      // Thumb Map Image path with extension jpeg|png
				imagePreset,             // ImagePreset
				activityType,            // Activity Type * (for color if color is empty)
				activityColor,
				labelActivityStats, // Label Activiy Stats
				labelActivityDate,  // Label Acitivy Date
				mapID,              // xid
				mapTexts,
			)

			fmt.Println("THUMB CREATE ::: ", k)

			createThumb(
				gpxDoc.Tracks,
				mapThumbFilePath,
				imagePreset,
				activityType,
				mapID,
			)
		}
	}

	/**
	 * TODO:
	 * - [x] CreateMpas must return: activityType,
	 * - [] The function createThumb could generate the thumb image later than the sql statements; the
	 */

	// return nil
}

func getPrivacyFromPrivacyType(privacy []apiv9.PrivacyType) (bool, bool, bool) {
	var isPrivacyPrivate, isPrivacyPublic, isPrivacyCommunity bool
	if privacy == nil {
		isPrivacyPrivate = false
		isPrivacyPublic = true
		isPrivacyCommunity = true
	} else {
		for _, el := range privacy {
			if el == apiv9.PrivacyType_Private {
				isPrivacyPrivate = true
				isPrivacyCommunity = false
				isPrivacyPublic = false
				return isPrivacyPrivate, isPrivacyPublic, isPrivacyCommunity
			}
			if el == apiv9.PrivacyType_Community {
				isPrivacyPrivate = false
				isPrivacyCommunity = true
			}
			if el == apiv9.PrivacyType_Community {
				isPrivacyPrivate = false
				isPrivacyPublic = true
			}
		}
	}

	return isPrivacyPrivate, isPrivacyPublic, isPrivacyCommunity
}

var vincenty = geo.Vincenty{
	Name:                          "Vincenty_SD",
	ShouldStandardDeviationBeUsed: true,
	SigmaMultiplier:               2.57583, // - 99%; 3.29053, // - 99.9%
	OneDegree:                     1000.0 * 10000.8 / 90.0,
	EarthRadius:                   6378137, // WGS-84 ellipsoid; See https://en.wikipedia.org/wiki/World_Geodetic_System
	Flattening:                    1 / 298.257223563,
	SemiMinorAxisB:                6356752.314245,
	Epsilon:                       1e-12,
	MaxIterations:                 200,
}

func readGpxFile(filePath string, mapID string) (*geo.GPX, error) {
	return gpxs.ParseFile(filepath.Join(filePath), &vincenty)
}
