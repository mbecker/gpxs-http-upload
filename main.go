package main

import (
	"errors"
	"fmt"
	"io/ioutil"
	"net/http"
	"os"
	"path/filepath"

	"github.com/alexsasharegan/dotenv"
	"github.com/golang/freetype/truetype"
	"github.com/julienschmidt/httprouter"
	_ "github.com/lib/pq"
	"github.com/rs/xid"
	"github.com/rs/zerolog/log"
	gpxsamqplib "gitlab.com/mbecker/gpxs-amqp-lib"
	"gitlab.com/mbecker/gpxs-http-upload/utils"
	apiv9 "gitlab.com/mbecker/gpxs-proto/apiv9"
)

var applicationName = "gpxs-http-upload"
var applicationID string
var currentDirectory string
var uploadDirectory string

// Maps
var mapDirectory string

// Fonts
const fontSubDirectory string = "ttf"

var fontDirectory string

// var queue *amqpqueue.Queue
var queue *gpxsamqplib.Queue
var envParams EnvParam

type EnvParam struct {
	// AmqpUser        string
	// AmqpPassword    string
	// AmqpURL         string
	// AmqpQueue       string
	UploadDirectory string
	MapsDirectory   string
	// DbConnection    string
	HTPPPort string
}

// IsValid checks if the struct EnvParam is valid
func (env *EnvParam) isValid() (bool, []error) {
	errorsSlice := []error{}
	isValid := true
	// if len(env.HTPPPort) == 0 {
	// 	errorsSlice = append(errorsSlice, errors.New("DB_CONNECTION not set"))
	// 	isValid = false
	// }
	// if len(env.DbConnection) == 0 {
	// 	errorsSlice = append(errorsSlice, errors.New("DB_CONNECTION not set"))
	// 	isValid = false
	// }
	// if len(env.AmqpUser) == 0 {
	// 	errorsSlice = append(errorsSlice, errors.New("AMQP_USER not set"))
	// 	isValid = false
	// }
	// if len(env.AmqpPassword) == 0 {
	// 	errorsSlice = append(errorsSlice, errors.New("AMQP_PASSWORD not set"))
	// 	isValid = false
	// }
	// if len(env.AmqpURL) == 0 {
	// 	errorsSlice = append(errorsSlice, errors.New("AMQP_URL not set"))
	// 	isValid = false
	// }
	// if len(env.AmqpQueue) == 0 {
	// 	errorsSlice = append(errorsSlice, errors.New("AMQP_QUEUE not set"))
	// 	isValid = false
	// }
	// if len(env.AmqpQueue) == 0 {
	// 	errorsSlice = append(errorsSlice, errors.New("AMQP_QUEUE not set"))
	// 	isValid = false
	// }
	if len(env.UploadDirectory) == 0 {
		errorsSlice = append(errorsSlice, errors.New("UPLOAD_DIRECTORY not set"))
		isValid = false
	}
	if len(env.MapsDirectory) == 0 {
		errorsSlice = append(errorsSlice, errors.New("MAPS_DIRECTORY not set"))
		isValid = false
	}
	return isValid, errorsSlice
}

func failOnError(err error, msg string) {
	if err != nil {
		log.Fatal().Err(err).Msg(msg)
	}
}

func failOnErrors(key string, errs []error, msg string) {
	if len(errs) > 0 {
		log.Fatal().Errs(key, errs).Msg(msg)
	}
}

func init() {
	applicationID = xid.New().String()
	log.Logger = log.With().Timestamp().Str("applicationName", applicationName).Str("applicationID", applicationID).Logger() // Caller()
	// zerolog.TimeFieldFormat = "" -- Use for timestamp
	log.Info().Msg("Initialized application started")

	// Initialize environment paramaeters
	err := dotenv.Load()
	if err != nil {
		failOnError(err, "dotenv error")
	}
	// envParams.AmqpURL = os.Getenv("AMQP_URL")
	// envParams.AmqpUser = os.Getenv("AMQP_USER")
	// envParams.AmqpPassword = os.Getenv("AMQP_PASSWORD")
	// envParams.AmqpQueue = os.Getenv("AMQP_QUEUE")
	envParams.UploadDirectory = os.Getenv("UPLOAD_DIRECTORY")
	// envParams.DbConnection = os.Getenv("DB_CONNECTION")
	envParams.HTPPPort = os.Getenv("HTTP_PORT")
	envParams.MapsDirectory = os.Getenv("MAPS_DIRECTORY")

	if isValid, errs := envParams.isValid(); isValid != true {
		failOnErrors("errors", errs, "ENV Parameters must be set")
	}

	// Initialize current directory
	currentDirectory, err = os.Getwd()
	failOnError(err, "The current directory couldn't be loaded")

	uploadDirectory = filepath.Join(currentDirectory, envParams.UploadDirectory)
	err = utils.CreateDirIfNotExist(uploadDirectory)
	failOnError(err, fmt.Sprintf("The directoy '%s' couldn't be created", uploadDirectory))

	mapDirectory = filepath.Join(envParams.MapsDirectory)
	err = utils.CreateDirIfNotExist(mapDirectory)
	failOnError(err, fmt.Sprintf("The directoy '%s' couldn't be created", mapDirectory))

	// FONTS
	fontDirectory = filepath.Join(currentDirectory, fontSubDirectory)
	fontActivityTypeFilePath := filepath.Join(fontDirectory, "FTY DELIRIUM NCV.ttf")
	fontActivityStatsTypeFilePath := filepath.Join(currentDirectory, "ttf", "Roboto-Black.ttf")
	fontActivityDateFilePath := filepath.Join(currentDirectory, "ttf", "Roboto-Light.ttf")
	fontLogoFilePath := filepath.Join(currentDirectory, "ttf", "Roboto-Bold.ttf")
	// Font: Activity Type
	fontFile, err := ioutil.ReadFile(fontActivityTypeFilePath)
	if err != nil {
		failOnError(err, "The font 'activity' couldn't be loaded")
	}
	fontActivityType, err := truetype.Parse(fontFile)
	if err != nil {
		failOnError(err, "The font 'activity' couldn't be parsed")
	}
	// Font: Activity Stats
	fontFile, err = ioutil.ReadFile(fontActivityStatsTypeFilePath)
	if err != nil {
		failOnError(err, "The font 'stats' couldn't be loaded")
	}
	fontActivityStats, err := truetype.Parse(fontFile)
	if err != nil {
		failOnError(err, "The font 'stats' couldn't be parsed")
	}

	// Font: Activity Data
	fontFile, err = ioutil.ReadFile(fontActivityDateFilePath)
	if err != nil {
		failOnError(err, "The font 'date' couldn't be loaded")
	}
	fontActivityDate, err := truetype.Parse(fontFile)
	if err != nil {
		failOnError(err, "The font 'stats' couldn't be parsed")
	}

	// font: Logo
	fontFile, err = ioutil.ReadFile(fontLogoFilePath)
	if err != nil {
		failOnError(err, "The font 'logo' couldn't be loaded")
	}
	fontLogo, err := truetype.Parse(fontFile)
	if err != nil {
		failOnError(err, "The font 'logo' couldn't be parsed")
	}

	for index := 0; index < len(ImagePresets); index++ {
		ImagePresets[index].fontActivityType = fontActivityType
		ImagePresets[index].fontActivityStats = fontActivityStats
		ImagePresets[index].fontActivityDate = fontActivityDate
		ImagePresets[index].fontLogo = fontLogo
	}

	log.Info().Msg("Initialized application finished")
}

func main() {
	// Initialize DB connection
	// db, err := sql.Open("postgres", envParams.DbConnection)
	// db.SetMaxOpenConns(10)
	// db.SetMaxIdleConns(10)
	// failOnError(err, "DB Connection couldn't be established")
	// defer db.Close()
	// err = db.Ping()
	// failOnError(err, "DB Connection Ping error")

	// Initialize AMQP Connection
	// amqpConnectionString := fmt.Sprintf("amqp://%s:%s@%s/", envParams.AmqpUser, envParams.AmqpPassword, envParams.AmqpURL)

	// queue = gpxsamqplib.New(envParams.AmqpQueue, amqpConnectionString)
	// defer queue.Close()

	cMiddleware := chainMiddleware(withCorsEnabled, jwtAuth)
	// dMiddleware := chainMiddleware(withCorsEnabled)

	router := httprouter.New()

	router.POST("/upload", cMiddleware(upload()))
	router.OPTIONS("/upload", cMiddleware(upload()))
	// router.GET("/status", dMiddleware(statusRoute()))
	router.ServeFiles("/maps/*filepath", http.Dir("./maps"))

	router.GET("/", func(w http.ResponseWriter, r *http.Request, ps httprouter.Params) {
		http.FileServer(http.Dir(("./static"))).ServeHTTP(w, r)
	})
	// router.GET("/maps", func(w http.ResponseWriter, r *http.Request, ps httprouter.Params) {
	// 	http.FileServer(http.Dir(("./maps"))).ServeHTTP(w, r)
	// })

	// http.HandleFunc("/upload", cMiddleware(upload))
	// http.ListenAndServe(":8080", nil) // setting listening port

	if err := http.ListenAndServe(":"+envParams.HTPPPort, router); err != nil {
		log.Error().Err(err).Msg("http ListenAndServe error")
	} else {
		log.Info().Str("http", "start").Msg("HTTP server listening on port: " + envParams.HTPPPort)
	}

}

func sendMesseage(
	gpxID int64,
	xidMap xid.ID,
	uuid string,
	filePath string,
	imagePreset apiv9.ImagePresetType,
	tileprovider apiv9.TileProviderType,
	activityType apiv9.ActivityType,
	activityName string,
	privacyType []apiv9.PrivacyType,
	mapTexts MapTexts,
) {

	gpxMessage, err := utils.GpxMessage(gpxID, xidMap, uuid, filePath, activityName, activityType, imagePreset, tileprovider, privacyType)
	if err != nil {
		log.Error().Err(err).Str("xid", xidMap.String()).Str("uuid", uuid).Str("name", "upload").Msg("Protomessage couldn't be created")
	}

	fmt.Println("--- (AMQP Message Parameters) ----")
	fmt.Println(activityName)
	fmt.Println(activityType)
	fmt.Println(privacyType)
	fmt.Println(imagePreset)
	fmt.Println(tileprovider)
	fmt.Println("----------")

	go handleGpxProcess(gpxMessage, mapTexts)

	// err = queue.Push(gpxMessage)
	// err = queue.UnsafePush(gpxMessage)
	// if err != nil {
	// 	log.Error().Err(err).Str("xid", xidMap.String()).Str("uuid", uuid).Str("name", "upload").Msg("AMQP Message couldn't be published")
	// } else {
	// 	log.Info().Str("xid", xidMap.String()).Str("uuid", uuid).Str("name", "amqp").Str("action", "sent").Str("status", "success").Msg("AMQP Message sent")
	// }
}
