package main

import (
	trueytpe "github.com/golang/freetype/truetype"
	"gitlab.com/mbecker/gpxs-proto/apiv9"
)

// ImagePreset defines the preset for a specif app like iPhone resolution, Instagram, etc.
type ImagePreset struct {
	Name string

	fontActivityType        *trueytpe.Font
	fontActivityTypeOptions *trueytpe.Options

	fontActivityStats        *trueytpe.Font
	fontActivityStatsOptions *trueytpe.Options

	fontActivityDate            *trueytpe.Font
	fontActivityDateOptions     *trueytpe.Options
	shouldActivityDateBePrinted bool

	fontLogo        *trueytpe.Font
	fontLogoOptions *trueytpe.Options

	imageWidth         float64
	imageHeight        float64
	imagePaddingTop    float64
	imagePaddingBottom float64
	imagePaddingRight  float64
	imagePaddingLeft   float64

	resizedImage ResizedImage
	thumbImage   ResizedImage

	pathWeight   float64
	tileProvider TileProvider
	imageType    ImageType
}

// GetImagePreset gets the ImagePreset Object/struct based on the given type of ImagePreset
func GetImagePreset(imagePresetType apiv9.ImagePresetType) ImagePreset {
	switch imagePresetType {
	case apiv9.ImagePresetType_iPhone8:
		return ImagePresets[0]
	case apiv9.ImagePresetType_iPhone8Plus:
		return ImagePresets[1]
	case apiv9.ImagePresetType_iPadPro129inch:
		return ImagePresets[2]
	case apiv9.ImagePresetType_Instagram:
		return ImagePresets[3]
	default:
		return ImagePresets[0]
	}
}

// ImagePresets contains all ImagePreset obects/structs for the different ImagePresetTypes
var ImagePresets = [...]ImagePreset{
	ImagePreset{
		Name: "iPhone8", // iPhone 8 (iPhone 6, iPhone 6s, iPhone 7): 750 x 1334

		imageWidth:         750.0,
		imageHeight:        1334.0,
		imagePaddingTop:    667.0 + 0.3*670.0,
		imagePaddingBottom: 72.0,
		imagePaddingRight:  72.0,
		imagePaddingLeft:   96.0,

		resizedImage: ResizedImage{
			width:  337.0,
			height: 600.0,
		},
		thumbImage: ResizedImage{
			width:  124.0,
			height: 124.0,
		},

		tileProvider: TileProviders[0],

		fontActivityTypeOptions: &trueytpe.Options{Size: 112},

		fontActivityStatsOptions: &trueytpe.Options{Size: 56},

		fontActivityDateOptions:     &trueytpe.Options{Size: 12},
		shouldActivityDateBePrinted: false,

		fontLogoOptions: &trueytpe.Options{Size: 64},

		pathWeight: 8.0,
		imageType:  ImageTypePng,
	},
	ImagePreset{
		Name: "iPhone8Plus", // iPhone 8 Plus (iPhone 6 Plus, iPhone 6s Plus, iPhone 7 Plus): 1242 x 2208

		imageWidth:         1242.0,
		imageHeight:        2208.0,
		imagePaddingTop:    48.0,
		imagePaddingBottom: 48.0,
		imagePaddingRight:  48.0,
		imagePaddingLeft:   48.0,

		tileProvider: TileProviders[0],

		fontActivityTypeOptions: &trueytpe.Options{Size: 112},

		fontActivityStatsOptions: &trueytpe.Options{Size: 56},

		fontActivityDateOptions:     &trueytpe.Options{Size: 12},
		shouldActivityDateBePrinted: false,

		fontLogoOptions: &trueytpe.Options{Size: 64},

		pathWeight: 8.0,
		imageType:  ImageTypePng,
	},
	ImagePreset{
		Name: "iPadPro129inch", // iPhone 8 (iPhone 6, iPhone 6s, iPhone 7): 750 x 1334

		imageWidth:         2732.0,
		imageHeight:        2048.0,
		imagePaddingTop:    48.0,
		imagePaddingBottom: 48.0,
		imagePaddingRight:  48.0,
		imagePaddingLeft:   48.0,

		tileProvider: TileProviders[0],

		fontActivityTypeOptions: &trueytpe.Options{Size: 112 * 1.5},

		fontActivityStatsOptions: &trueytpe.Options{Size: 561.5},

		fontActivityDateOptions:     &trueytpe.Options{Size: 12},
		shouldActivityDateBePrinted: false,

		fontLogoOptions: &trueytpe.Options{Size: 64 * 1.5},

		pathWeight: 8.0 * 1.5,
		imageType:  ImageTypePng,
	},
	ImagePreset{
		Name: "instagram",

		imageWidth:         1080.0,
		imageHeight:        1080.0,
		imagePaddingTop:    48.0,
		imagePaddingBottom: 48.0,
		imagePaddingRight:  48.0,
		imagePaddingLeft:   48.0,

		resizedImage: ResizedImage{
			width:  600.0,
			height: 600.0,
		},
		thumbImage: ResizedImage{
			width:  124.0,
			height: 124.0,
		},

		tileProvider: TileProviders[0],

		fontActivityTypeOptions: &trueytpe.Options{Size: 112},

		fontActivityStatsOptions: &trueytpe.Options{Size: 56},

		fontActivityDateOptions:     &trueytpe.Options{Size: 12},
		shouldActivityDateBePrinted: false,

		fontLogoOptions: &trueytpe.Options{Size: 64},

		pathWeight: 8.0,
		imageType:  ImageTypeJpeg,
	},
}
