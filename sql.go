package main

import (
	"database/sql"
	"fmt"
)

func sqlInsert(
	db *sql.DB,
	xidMap string,
	uuid string,
	fileName string,
	filePath string,
) (int64, error) {
	var gpxID int
	err := db.QueryRow(`
		INSERT INTO strava_gpxs
			(
				uuid, filename, filepath
			)
		VALUES
			(
				$1, $2, $3
			)
		RETURNING gpxid
	`, uuid, fileName, filePath,
	).Scan(&gpxID)

	gpxID64 := int64(gpxID)
	// if gpxID == 0 {
	// 	return gpxID, errors.New("Insert DB gpx was not successful")
	// }
	if err != nil {
		return gpxID64, err
	}

	// sqlStatements sould have all SQL statements which shoud be executed in one transaction
	sqlStatements := fmt.Sprintf(`
	INSERT INTO strava_maps
	(
		xid, gpxid, uuid, filename, public, community, private
	) 
	VALUES (
		'%s', %d, '%s', '%s', %t, %t, %t
		);`,
		xidMap, gpxID, uuid, fileName, false, false, true,
	)

	if _, err := db.Exec(sqlStatements); err != nil {
		return gpxID64, err
	}
	return gpxID64, nil
}
